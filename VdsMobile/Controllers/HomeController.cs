﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using HtmlAgilityPack;
using System.Linq;

namespace VdsMobile.Controllers
{
    public class HomeController : Controller
    {
        private readonly ITeamRepository _repository = new TeamRepository();

        [OutputCache(Duration = 30, VaryByParam = "*")]
        public ActionResult Index(string team = null)
        {
            Team model = null;
            if (!string.IsNullOrEmpty(team))
            {
                model = _repository.GetTeamDetails(team);
            }

            return View(model);
            
        }
    }

    public class TeamRepository : ITeamRepository
    {
        private readonly Cache _cache = HttpRuntime.Cache;
        private static readonly object Lock = new object();
        readonly DateTime _cacheAbsoluteDuration = DateTime.Now.AddSeconds(30);
        private const string PdcvdsUrl = "http://pdcvds.com/teams.php";

        public Team GetTeamDetails(string teamName)
        {
            var teams = LoadTeamsFromCache();
            var teamInfo = GetTeam(teamName, teams);
            return teamInfo;
        }

        private Team GetTeam(string teamName, Dictionary<string, Team> teams)
        {
            var matchingTeam = teams.FirstOrDefault(x => x.Key.StartsWith(teamName.ToLower()));
            var teamInfo = matchingTeam.Value;
            if (teamInfo != null) 
            {
                teamInfo.Riders.AddRange(GetTeamRiders(teamInfo.Name, teamInfo.Url));
            }
            else if (!string.IsNullOrEmpty(teamName))
            {
                teamInfo = new Team {Name = string.Format("Team '{0}' not found.", teamName)};
            }
            else if (string.IsNullOrEmpty(teamName))
            {
                teamInfo = new Team { Name = string.Format("Please enter team name.") };
            }
            return teamInfo;
        }

        private IEnumerable<Rider> GetTeamRiders(string teamName, string teamUrl)
        {
            var key = teamName.ToLower();
            var riders = _cache.Get(key) as List<Rider>;
            if (riders == null)
            {
                riders = new List<Rider>();
                
                var web = new HtmlWeb();
                var urlDecode = HttpUtility.UrlDecode(teamUrl);
                if (urlDecode != null)
                {
                    var userUrl = PdcvdsUrl + urlDecode.Replace("&amp;", "&");
                    var htmlDoc = web.Load(userUrl);
                    var riderNodes = htmlDoc.DocumentNode.SelectNodes("//*[@id=\"content\"]/table[1]/tr");
                    foreach (var riderNode in riderNodes)
                    {
                        if (riderNode.SelectSingleNode("td") != null)
                        {
                            var riderName = riderNode.SelectSingleNode("td[5]/a").InnerText;
                            var price = Convert.ToInt32(riderNode.SelectSingleNode("td[6]/a").InnerText);
                            var score = Convert.ToInt32(riderNode.SelectSingleNode("td[8]").InnerText);
                            var rider = new Rider {Name = riderName, Price = price, Score = score};
                            riders.Add(rider);
                        }
                    }
                }

                _cache.Add(key, riders, null, _cacheAbsoluteDuration, Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
            }

            return riders;
        }

        private Dictionary<string, Team> LoadTeamsFromCache()
        {
            Dictionary<string, Team> result;

            const string key = "riders";
            if (_cache.Get(key) == null)
            {
                lock (Lock)
                {
                    if (_cache.Get(key) == null)
                    {
                        result = new Dictionary<string, Team>();

                        const string url = "http://pdcvds.com/teams.php?mw=1&y=2014";
                        var web = new HtmlWeb();
                        HtmlDocument htmlDoc = web.Load(url);

                        var ridersHtml = htmlDoc.DocumentNode.SelectNodes("//div[@id=\"content\"]/table/tr");
                        foreach (var item in ridersHtml)
                        {
                            // ingore first row
                            if (item.SelectSingleNode("td") != null)
                            {
                                var linkNode = item.SelectSingleNode("td[3]/a");
                                var teamName = linkNode.InnerText;
                                var teamUrl = linkNode.GetAttributeValue("href", string.Empty);
                                var points = item.SelectSingleNode("td[4]").InnerText;
                                result.Add(teamName.ToLower(),
                                           new Team {Url = teamUrl, Loaded = false, Points = Convert.ToInt32(points), Name = teamName});
                            }
                        }
                        _cache.Add(key, result, null, DateTime.Now.AddSeconds(30), Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
                    }
                    else
                    {
                        result = (Dictionary<string, Team>)_cache[key];
                    }
                }
            }
            else
            {
                result = (Dictionary<string, Team>) _cache[key];
            }
            return result;
        }
    }

    public class Rider
    {
        public string Name { get; set; }
        public int Price { get; set; }
        public int Score { get; set; }
    }

    public interface ITeamRepository
    {
        Team GetTeamDetails(string team);
    }

    public class Team
    {
        public Team()
        {
            Riders = new List<Rider>();
        }

        public string Name { get; set; }
        public string Url { get; set; }
        public bool Loaded { get; set; }
        public int Points { get; set; }
        public List<Rider> Riders { get; private set; }
    }
}
